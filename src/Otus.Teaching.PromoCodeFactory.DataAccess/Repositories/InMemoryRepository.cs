﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }
       // private static List<T> NewData { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
         //   Data = NewData ?? new List<T>(data);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> Create(T entity)
        {
            Data.Add(entity);
           // NewData = Data ?? Data.ToList();
            return Task.FromResult(entity.Id);
        }

        public Task<Guid> Update(T entity)
        {
            Data = Data.Where(x => x.Id != entity.Id).Append(entity).ToList();
            //NewData = Data ?? Data.ToList();
            return Task.FromResult(entity.Id);
        }

        public Task Delete(Guid id)
        {
            Data.RemoveAll(x => x.Id == id);
            //NewData = Data ?? Data.ToList();
            return Task.CompletedTask;
        }

        public Task<bool> Find(Guid id)
        {
            return Task.FromResult(Data.Any(x => x.Id == id));
        }
    }
}
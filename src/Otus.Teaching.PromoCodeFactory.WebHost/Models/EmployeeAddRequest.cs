﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeAddRequest
    {
        [Required(ErrorMessage = "Обязательное поле")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        public string Email { get; set; }
        public List<Guid>? Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}
